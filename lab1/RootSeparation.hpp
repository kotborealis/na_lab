#pragma once

#include <vector>
#include <utility>
#include <algorithm>

#include "PolynomialBounds.hpp"

// Отделяет корни на участке с заданной точностью
std::vector<std::pair<double, double>> rootSeparation(double (*f)(double), 
    std::pair<int, int> bounds, double precision){
    std::vector<std::pair<double, double>> bounds_v;

    // перебор всех отрезков с шагом precision
    for(double left = 1.f * bounds.first; left < bounds.second * 1.f; left += precision){
        double right = left + precision;
        // Если левая граница --- корень
        if(f(left) == 0){
            bounds_v.push_back(std::make_pair(left, left));
        }
        // Правая граница --- корень
        if(f(right) == 0){
            bounds_v.push_back(std::make_pair(right, right));
        }
        // Между границ лежит корень
        if(f(left) * f(right) < 0){
            bounds_v.push_back(std::make_pair(left, right));
        }
    }

    // отсортируем и уберём дупликаты
    std::sort(bounds_v.begin(), bounds_v.end());
    bounds_v.erase(std::unique(bounds_v.begin(), bounds_v.end()), bounds_v.end());

    return bounds_v;
}