#include <cassert>
#include "Horner.hpp"

int main(int argc, char** argv){
    assert(horner_eval({2, -4, 5, -2}, 3) == 31);

    std::vector<double> h_cs_expected({2, 2, 11, 31});
    std::vector<double> h_cs_actuall = horner_cs({2, -4, 5, -2}, 3);
    assert(h_cs_actuall == h_cs_expected);
}