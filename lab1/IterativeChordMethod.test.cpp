#include <math.h>
#include <utility>
#include <assert.h>
#include "IterativeChordMethod.hpp"
#include <iostream>

int main(int argc, char** argv){
    double (*f1)(double) = [](double x) -> double {return x*x*x + 4 * x - 6;};
    double (*f1d)(double) = [](double x) -> double {return 3*x*x + 4;};
    double (*f1dd)(double) = [](double x) -> double {return 3*x;};

    assert(fabs(iterativeChordMethod(f1, f1d, f1dd, std::make_pair(-1, 1), 0.0001) - 1.1347) < 0.0001);
}