#pragma once

#include <cstdlib>
#include <vector>

// Высчитывает многочлен P_n(x), заданный коэффициентами cs, в точке x
double horner_eval(std::vector<double> cs, double x){
    double ret = 0;

    for(auto c : cs){
        ret = c + ret * x;   
    }

    return ret;
}

// Высчитывает коэффициенты горнера для P_n(x) в точке x
std::vector<double> horner_cs(std::vector<double> cs, double x){
    std::vector<double> ret;
    ret.push_back(cs[0]);

    for(int i = 1; i < cs.size(); i++){
        ret.push_back(cs[i] + ret.back() * x);
    }

    return ret;
}