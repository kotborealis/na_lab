#pragma once

#include <vector>
#include <utility>
#include <iostream>

#include "Horner.hpp"

// Возвращает верхнюю границу
// Действительных корней многочлена
int HornerUpperBound(std::vector<double> cs){
    int x = 0;
    bool iterate = true;

    // Поиск такого икс
    while(iterate){
        x++;
        // что коэффициенты горнера
        auto h_cs = horner_cs(cs, x);
        // больше нуля
        bool all_positive = true;
        for(auto c : h_cs){
            if(c <= 0){
                all_positive = false;
                break;
            }
        }
        iterate = !all_positive; // если все >0, заканчиваем цикл
    }

    return x;
}

// Возвращает пару, которая содержит 
// нижнюю и верхнюю границу действительных корней многочлена
std::pair<int, int> getPolynomialBounds(std::vector<double> cs){
    // нормализованные коэффициенты 
    // (если первый коэффициент отрицательный, умножить все на -1)
    std::vector<double> normalized_cs;
    int normalization_c = (cs[0] < 0) ? -1 : 1;
    for(auto c : cs)
        normalized_cs.push_back(c * normalization_c);

    // многочлен (-1)^n P_n(-x) 
    std::vector<double> alternated_cs;
    for(int i = 0; i < cs.size(); i++)
        alternated_cs.push_back(normalized_cs[i] * (1 - 2 * (i % 2)));

    return std::make_pair(
        -HornerUpperBound(alternated_cs), 
        HornerUpperBound(normalized_cs)
    );
}