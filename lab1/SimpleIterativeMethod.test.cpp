#include "SimpleIterativeMethod.hpp"
#include <assert.h>
#include <math.h>
#include <iostream>

int main(int argc, char** argv){
    double (*f1)(double) = [](double x) -> double {return 0.25f * (6 - x*x*x);};

    assert(fabs(simpleIterativeMethod(f1, 0, 0.0001) - 1.1347) < 0.0001);
    assert(fabs(simpleIterativeMethod(f1, 0, 0.001) - 1.1347) < 0.001);
}