#include <assert.h>
#include <stdio.h>
#include <utility>
#include "PolynomialBounds.hpp"

int main(int argc, char** argv){
    std::vector<double> p1 = {2., -4., 5., -2.}; auto b1 = std::make_pair(-1, 3);
    std::vector<double> p2 = {1., 80., -80.}; auto b2 = std::make_pair(-81, 1);
    std::vector<double> p3 = {1., 0., 4., -6.}; auto b3 = std::make_pair(-1, 2);

    auto b = getPolynomialBounds(p1);
    assert(b == b1);
    b = getPolynomialBounds(p2);
    assert(b == b2);
    b = getPolynomialBounds(p3);
    assert(b == b3);
}