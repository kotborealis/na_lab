#include <math.h>

// Метод ньютона
// f - функция, fd, fdd - первая и вторая производные
// range - отрезок, на котором лежит корень
// epsilon - точность
double newtonsMethod(double (*f)(double), double (*fd)(double), 
    double (*fdd)(double), std::pair<double, double> range, 
    double epsilon){

    auto a = range.first;
    auto b = range.second;

    // знак произведения производных
    double dm_sign = fd((a+b)/2) * fdd((a+b)/2);
    dm_sign = (dm_sign > 0) - (dm_sign < 0);
    
    double x_0 = (dm_sign > 0) ? b : a;
    double x_1 = x_0;

    do{
        x_0 = x_1;
        x_1 = x_0 - f(x_0) / fd(x_0);
    }
    while(fabs(x_0 - x_1) > epsilon);

    return x_1;
}