#include <assert.h>
#include <vector>
#include <utility>
#include <math.h>

#include "RootSeparation.hpp"

int main(int argc, char** argv){
    double (*f1)(double) = [](double x) -> double {return x*x - sin(x);};
    std::vector<std::pair<double, double>> r1 = {std::make_pair(0, 0), std::make_pair(0.5, 1)};

    double (*f2)(double) = [](double x) -> double {return x*x*x - 6*x + 2;};
    std::vector<std::pair<double, double>> r2 = {std::make_pair(-3, -2), std::make_pair(0, 1), std::make_pair(2, 3)};

    auto r = rootSeparation(f1, std::make_pair(-1, 1), 0.5f);
    assert(r == r1);
    r = rootSeparation(f2, std::make_pair(-10, 10), 1.f);
    assert(r == r2);
}