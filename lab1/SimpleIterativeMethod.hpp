#include <math.h>

// Метод простых итераций
// f - функция, x_0 - первое приближение
// epsilon - точность
double simpleIterativeMethod(double (*f)(double), double x_0, double epsilon){
    double x_1 = f(x_0);
    if(fabs(x_1 - x_0) < epsilon){
        return x_1;
    }

    // tail-recursion
    return simpleIterativeMethod(f, x_1, epsilon);
}