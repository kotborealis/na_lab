#include <math.h>

// Итеративный метод хорд
// f - функция, fd, fdd - первая и вторая производные
// range - отрезок, на котором лежит корень
// epsilon - точность
double iterativeChordMethod(double (*f)(double), double (*fd)(double), 
    double (*fdd)(double), std::pair<double, double> range, 
    double epsilon){

    auto a = range.first;
    auto b = range.second;

    // знак произведения производных
    double dm_sign = fd((a+b)/2.f) * fdd((a+b)/2.f);
    dm_sign = dm_sign < 0 ? -1 : 1;

    // Фиксированная точка
    double fixed_point = (dm_sign > 0) ? b : a;
    double x_0 = -f(a) / (f(b) - f(a)) * (b - a);
    double x_1 = x_0;


    do{
        x_0 = x_1;
        // две ветки для dm_sign >< 0 объединены в одну формулу 
        x_1 = x_0 - f(x_0) / (dm_sign * (f(fixed_point) - f(x_0))) 
              * dm_sign * (fixed_point - x_0);
    }
    while(fabs(x_0 - x_1) > epsilon);

    return x_1;
}