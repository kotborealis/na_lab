#include <math.h>
#include <iostream>

// Комбинированный метод (ньютон и хорды)
// f - функция, fd, fdd - первая и вторая производные
// range - отрезок, на котором лежит корень
// epsilon - точность
double combinedMethod(double (*f)(double), double (*fd)(double), 
    double (*fdd)(double), std::pair<double, double> range, 
    double epsilon){

    auto a = range.first;
    auto b = range.second;
    auto a_ = a;
    auto b_ = b;

    // знак произведения производных
    double dm_sign = fd((a+b)/2.f) * fdd((a+b)/2.f);
    dm_sign = dm_sign < 0 ? -1 : 1;

    do{
        a = a_;
        b = b_;
        // Две ветки для разных знаков производных
        if(dm_sign > 0){
            a_ = a - f(a) / (f(b) - f(a)) * (b - a);
            b_ = b - f(b) / fd(b);
        }
        else{
            a_ = a - f(a) / fd(a);
            b_ = b - f(b) / (f(b) - f(a)) * (b - a);
        }
    }
    while(fabs(a_ - b_) > epsilon);

    // Уточнённый корень лежит посередине полученного отрезка
    return (a_ + b_) * .5f;
}