#include <iostream>
#include "./JacobiMethod.hpp"
#include <cassert>
#include <cmath>

void print(d_matrix A){
    for(auto i : A){
        for(auto j : i){
            std::cout<<j<<"\t";
        }
        std::cout<<"\n";
    }
}

void print(d_vector A){
    for(auto i : A)
        std::cout<<i<<"\t";
    std::cout<<"\n";
}

int main(int argc, char** argv){
    d_matrix A = {
        {4, 2, -1, 0.5},
        {1, -5, 2, 1},
        {2, 1, -4, -1.5},
        {1, -0.4, 0.8, -3},
    };
    d_vector b = {
        4.5,
        14,
        -27.5,
        -1.8
    };
    d_vector X = {
        2,
        1,
        7,
        3
    };

    double varepsilon = 0.001;
    auto X_ = jacobiMethod(A, b, varepsilon);
    auto e = eval(A, b, X_);

    print(X_);
    print(e);

    for(int i = 0; i < 4; i++){
        std::cout<<"Assert "<<e[i]<<" < "<<varepsilon<<"\n";
        assert(fabs(e[i]) < varepsilon);
    }

    return 0;
}