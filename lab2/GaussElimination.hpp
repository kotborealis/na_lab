#include <iostream>
#include <cmath>
#include <vector>
#include <utility>

typedef std::vector<std::vector<double>> d_matrix;
typedef std::vector<double> d_vector;

d_vector gaussElimination(d_matrix A, d_vector b){
    int n = A.size();

    for(int i = 0; i < n; i++){
        // сделаем 'partial pivoting'
        // т.е. преобразуем матрицу в нужный вид
        // меняя строки местами.
        // найдём разрешающий элемент
        // и строку, на которой он лежит
        int pivot_row = i;

        // есть ли под этим элементом что-то большее (по модулю)
        for(int k = i + 1; k < n; k++){
            if(fabs(A[pivot_row][i]) < fabs(A[k][i])){
                pivot_row = k;
            }
        }

        // Поставим строку с pivot на текущее место
        std::swap(A[i], A[pivot_row]);
        std::swap(b[i], b[pivot_row]);


        // Gauss elimination
        double r_pivot = 1.f / A[i][i];
        for(int j = i + 1 ; j < n; j++){ // проходим по всем следующим строкам
            // вычисляем коэффициент, на который
            // умножим строку i
            // и затем вычтем её из строки j
            double c = A[j][i] * r_pivot; 
            for(int k = 0; k < n; k++){ // по всем элементам
                A[j][k] -= A[i][k] * c; 
            }
            b[j] -= b[i] * c;
        }
    }

    // Обратный ход гаусса
    d_vector X(n);
    for(int i = n - 1; i >= 0; i--){ // проходим по строкам в обратном порядке
        // x_i = (b_i - \sum_{k = i}^{n-1} x_k * a_{ik}) / a_{ii}
        X[i] = b[i];
        for(int k = i + 1; k < n; k++){
            X[i] -= X[k] * A[i][k];
        }
        X[i] /= A[i][i];
    }

    return X;
}