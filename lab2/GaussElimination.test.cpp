#include <iostream>
#include "./GaussElimination.hpp"
#include <cassert>
#include <cmath>

void print(d_matrix A){
    for(auto i : A){
        for(auto j : i){
            std::cout<<j<<"\t";
        }
        std::cout<<"\n";
    }
}

void print(d_vector A){
    for(auto i : A)
        std::cout<<i<<"\t";
    std::cout<<"\n";
}

d_vector eval(d_matrix A, d_vector b, d_vector X){
    int n = A.size();
    d_vector e(n);

    for(int i = 0; i < n; i++){
        e[i] = -b[i];
        for(int j = 0; j < n; j++){
            e[i] += A[i][j] * X[j];
        }
    }

    return e;
}

int main(int argc, char** argv){
    d_matrix A = {
        {4, 2, -1, 0.5},
        {1, -5, 2, 1},
        {2, 1, -4, -1.5},
        {1, -0.4, 0.8, -3},
    };
    d_vector b = {
        4.5,
        14,
        -27.5,
        -1.8
    };
    d_vector X = {
        2,
        1,
        7,
        3
    };

    auto X_ = gaussElimination(A, b);
    auto e = eval(A, b, X_);

    print(X_);
    print(e);

    double varepsilon = 0.001;
    for(int i = 0; i < 4; i++){
        assert(fabs(e[i]) < varepsilon);
    }

    return 0;
}