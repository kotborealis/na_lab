#include <cmath>
#include <vector>

typedef std::vector<std::vector<double>> d_matrix;
typedef std::vector<double> d_vector;

// Вычисление системы уравнений
d_vector eval(d_matrix A, d_vector b, d_vector X){
    int n = A.size();
    d_vector e(n);

    for(int i = 0; i < n; i++){
        e[i] = -b[i];
        for(int j = 0; j < n; j++){
            e[i] += A[i][j] * X[j];
        }
    }

    return e;
}

// норма max a_i - b_i
bool leq_epsilon(d_vector a, d_vector b, double epsilon){
    for(int i = 0; i < a.size(); i++)
        if(fabs(a[i] - b[i]) >= epsilon)
            return false;

    return true;
}

// Jacobi method pass
d_vector jacobiMethodPass(d_matrix A, d_vector b, d_vector X_){
    int n = A.size();
    d_vector X(n);

    for(int i = 0; i < n; i++){
        double acc = 0;
        for(int j = 0; j < n; j++){
            if(j != i){
                acc += A[i][j] * X_[j];
            }
        }
        X[i] = 1.f / A[i][i] * (b[i] - acc);
    }

    return X;
}

d_vector jacobiMethod(d_matrix A, d_vector b, double epsilon){
    int n = A.size();
    d_vector y = b;
    d_vector X_ = b;
    d_vector X = b;

    // Jacobi method
    do{
        X_ = X;
        X = jacobiMethodPass(A, b, X_);
    }while(!leq_epsilon(X_, X, epsilon));

    // вычислим систему и проверим, получена ли заданная точность
    // если заданная точность не получена,
    // сделаем ещё один проход
    bool do_extra_pass = true;
    while(do_extra_pass){
        do_extra_pass = false;

        auto e = eval(A, b, X);
        for(auto e_i : e)
            if(fabs(e_i) > epsilon)
                do_extra_pass = true;

        if(do_extra_pass)
            X = jacobiMethodPass(A, b, X);
    }

    return X; // for now
}